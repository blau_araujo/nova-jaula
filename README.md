# Nova Jaula

Script para criação de novas jaulas de empacotamento .deb

## Atenção!

* **Este script foi criado para uso pessoal meu, e não há garantias de que ele funcione corretamente no seu sistema!**
* **Este script só deve ser utilizado por pessoas capazes de ler e entender o que código faz!**
* **Este script é apenas para usuários que conheçam bem o Debian!**
* **Se mal utilizado, este script pode causar danos irreparáveis ao seu sistema!**
* **O jeito certo de fazer login como usuário root é com o comando `su -`**

## Preparação

O script `jaula-nova.sh` requer os arquivos `jaula-pi.sh` e `jaula-bash.conf` estejam no mesmo local da pasta `/root`!

Por exemplo:

```
/root
  |
  +--- jaula-nova.sh
  |
  +--- jaula-bash.conf
  |
  +--- jaula-pi.sh
```

Depois de copiados para seu destino, torne apenas o script `jaula-nova.sh` executável:

```
:~# chmod +x jaula-nova.sh
```

## Uso

Como usuário **root** (`su -`), execute:

```
:~# ./jaula-nova.sh
```

Por padrão, a nova jaula será criada em `/root`, mas é possível indicar outro local de destino, por exemplo: `/home/usuario`. Este recurso é útil para uma partição raiz com pouco estaço.

Após a execução do script `jaula-nova.sh`, já no chroot, entre em `/root` (do `chroot`) e execute:

```
[CHROOT]:~# ./jaula-pi.sh
```

