#!/usr/bin/env bash

read -N 1 -p "
ESTE SCRIPT SÓ DEVE SER EXECUTADO DENTRO DA JAULA!

Continuar (s/N)? " continua

[[ ${continua,,} != 's' ]] && echo -e "\n\nSaindo...\n" && exit

# Este script instalará os seguintes pacotes

apt install \
build-essential \
autoconf \
automake \
autotools-dev \
dh-make \
debhelper \
devscripts \
fakeroot \
xutils \
lintian \
pbuilder \
bash-completion \
git \
tree \
wget \
locales

# Em seguida, você poderá configurar a localidade.

[[ $? -eq 0 ]] && dpkg-reconfigure locales || echo -e "\n\nErro! Saindo...\n"
