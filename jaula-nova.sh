#!/usr/bin/env bash

[[ $EUID -ne 0 && $HOME != '/root' ]] && echo "
Usuário não autorizado!
Este script deve ser executado apenas como 'root'!

Saindo...
" && exit 1

SCRIPTDIR=$(pwd)

[[ ! -f $SCRIPTDIR/jaula-bash.conf || ! -f $SCRIPTDIR/jaula-pi.sh ]] && \
echo "
Os arquivos 'jaula-bash.conf' e 'jaula-pi.sh' não foram
encontrados na mesma pasta deste script.

Saindo...
"

_quit() {
    echo -e "\n\nSaindo...\n"
    exit 0
}

read -p "
Informe o local de criação da jaula (vazio = /root): " destino

DESTINATION=${destino:-$HOME}

[[ ! -d $DESTINATION ]] && echo "Diretório $DESTINATION não existe!" && _quit

echo "
Este script criará uma nova jaula com a suíte Debian escolhida e nome 'jaula-SUITE'.
"
read -p "Informe a suíte a ser instalada: " suite

JAULANAME=jaula-$suite
JAULADIR=$DESTINATION/$JAULANAME

read -N 1 -p "- Iniciando o 'debootstrap' para instalar:

  Suíte: $suite
  Local: $JAULADIR

Continuar (S/n)? " continua

[[ ${continua,,} = 'n' ]] && _quit

echo "
- Criando a pasta '$JAULANAME'..."
mkdir -p $JAULADIR

echo "- Iniciando debootstrap...
"
debootstrap $suite $JAULADIR/ http://deb.debian.org/debian

(($?)) && _quit

echo "
- Criando pasta '/pkgs'..."

mkdir -p $JAULADIR/pkgs

echo "- Copiando scripts..."

cp $SCRIPTDIR/jaula-pi.sh $JAULADIR/root/
chmod +x $JAULADIR/root/jaula-pi.sh

echo "- Configurando o Bash da jaula...
"
cat $SCRIPTDIR/jaula-bash.conf | sed -e "s/%NAME%/$JAULANAME/" >> $JAULADIR/root/.bashrc

read -N 1 -p "Deseja fazer o chroot agora (S/n)? " continua

[[ ${continua,,} = 'n' ]] && _quit

chroot $JAULADIR

exit
